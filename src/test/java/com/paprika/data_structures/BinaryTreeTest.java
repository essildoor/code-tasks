package com.paprika.data_structures;


import org.junit.Test;

import java.util.Arrays;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Created by essildoor on 10/28/15.
 */
public class BinaryTreeTest {
    @Test
    public void testAsArray() throws Exception {
        int[] origin = new int[] {5, 4, 11, 3, 0, 12, 7, 2, 1};
        int[] expected = Arrays.copyOf(origin, origin.length);
        Arrays.sort(expected);
        BinaryTree bt = new BinaryTree(origin);
        int[] actual = bt.asArray();

        assertThat(expected, is(equalTo(actual)));
    }
}
