package com.paprika.strings;

import org.junit.Test;

import static com.paprika.strings.StringFun.*;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Created by essildoor on 10/12/15.
 */
public class StringFunTest {
    @Test
    public void testIsStringConsistsOfUniqueChars() throws Exception {
        assertThat(true, is(equalTo(isStringConsistsOfUniqueChars("abc"))));
        assertThat(false, is(equalTo(isStringConsistsOfUniqueChars("abcddf"))));
        assertThat(true, is(equalTo(isStringConsistsOfUniqueChars(""))));
    }

    @Test
    public void testReplaceAllSpaces() throws Exception {
        assertThat("123%20abc", is(equalTo(replaceAllSpaces("123 abc"))));
        assertThat("hello%20world!", is(equalTo(replaceAllSpaces("hello world!"))));
        assertThat("a%20b%20%20c%20%20%20d%20%20%20%20e%20%20%20%20%20f%20%20%20%20%20%20g",
                is(equalTo(replaceAllSpaces("a b  c   d    e     f      g"))));
        assertThat("123abc", is(equalTo(replaceAllSpaces("123abc"))));
    }

    @Test
    public void testRemoveDuplicates() throws Exception {
        assertThat("abc", is(equalTo(removeDupes("abcabc"))));
    }
}
