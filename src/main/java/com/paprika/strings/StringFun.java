package com.paprika.strings;

import java.util.Arrays;

/**
 * Created by essildoor on 10/12/15.
 */
public class StringFun {

    public static boolean isStringConsistsOfUniqueChars(final String str) {
        if (str == null) throw new IllegalArgumentException("String must not be null!");
        for (int i = 0; i < str.length(); i++) {
            for (int j = i + 1; j < str.length(); j++) {
                if (str.charAt(i) == str.charAt(j)) return false;
            }
        }
        return true;
    }

    public static String removeDuplicates(final String str) {

        return str;
    }

    public static String removeDupes(final String str){
        char[] arr = str.toCharArray();
        int len = arr.length;
        int tail = 1;
        for(int x = 1; x < len; x++){
            int y;
            for(y = 0; y < tail; y++){
                if (arr[x] == arr[y]) break;
            }
            if (y == tail){
                arr[tail] = arr[x];
                tail++;
            }
        }
        return new String(Arrays.copyOfRange(arr, 0, tail));
    }

    public static String replaceAllSpaces(final String source) {
        final String pattern = " ";
        final String replacement = "%20";
        return source.replaceAll(pattern, replacement);
    }
}
