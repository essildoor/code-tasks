package com.paprika.data_structures;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Created by essildoor on 10/27/15.
 */
public class BinaryTree {
    class Node {
        int value;
        Node left;
        Node right;

        public Node(int value) {
            this.value = value;
        }

        public Node(int value, Node left, Node right) {
            this.value = value;
            this.left = left;
            this.right = right;
        }
    }

    private Node root;

    /**
     * creates binary tree with one node
     * @param value int value for root node of the tree
     */
    public BinaryTree(int value) {
        root = new Node(value);
    }

    /**
     * creates binary tree from given array of int values
     * @param values array of int values
     */
    public BinaryTree(int[] values) {
        if (values == null || values.length == 0) {
            throw new IllegalArgumentException("bad values!");
        }
        root = new Node(values[0]);
        if (values.length > 1) {
            for (int i = 1; i < values.length; i++) {
                insert(values[i]);
            }
        }
    }

    /**
     * inserts int value into the tree
     * @param value int value to insert
     */
    public void insert(int value) {
        appendNode(root, new Node(value));
    }

    private void appendNode(Node currentNode, Node newNode) {
        if (newNode.value < currentNode.value) {
            if (currentNode.left == null) {
                currentNode.left = newNode;
            } else {
                appendNode(currentNode.left, newNode);
            }
        } else if (newNode.value > currentNode.value) {
            if (currentNode.right == null) {
                currentNode.right = newNode;
            } else {
                appendNode(currentNode.right, newNode);
            }
        }
    }

    public void delete(int value) {

    }

    private void deleteIn(Node root, int value) {
        if (root != null) {
            if (value < root.value) deleteIn(root.left, value);
            if (value > root.value) deleteIn(root.right, value);
            else {
                if (root.left == null && root.right == null) replaceNodeInParent();
                else if (root.left == null && root.right != null) root = root.right;
            }
        }
    }

    private void replaceNodeInParent(Node node) {

    }

    /**
     * traverses tree in natural order and places nodes values to array
     * @return an array representation of the tree
     */
    public int[] asArray() {
        List<Integer> out = new ArrayList<Integer>();
        traverse(out, root);
        int[] result = new int[out.size()];
        for (int i = 0; i < out.size(); i++) result[i] = out.get(i);
        return result;
    }

    private void traverse(List<Integer> out, Node node) {
        if (node != null) {
            traverse(out, node.left);
            out.add(node.value);
            traverse(out, node.right);
        }
    }
}
